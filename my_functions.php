<?php
function test()
{
    global $test_i;
    $test_i++;
    echo "TEST #" . $test_i;
    echo "<br>";
}

function head($title = "Document")
{ ?>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= $title ?></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
                integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                crossorigin="anonymous"></script>
    </head>
<?php } ?>
<?php
function json_open($file)
{
    $jsonString = file_get_contents("$file.json");
    $data = json_decode($jsonString, true);
    print_r($data[1]);
}

function json_save($array = "", $json_file_name = "json_data")
{
    $fp = fopen("$json_file_name.json", 'w');
    fwrite($fp, json_encode($array, JSON_PRETTY_PRINT));   //here it will print the array pretty
    fclose($fp);
}

function p($text)
{
    echo "$text";
    echo '<br>';
}

function p_b($text)
{
    echo "<b>$text</b>";
    echo '<br>';
}

function d($arr)
{
    debug($arr);
}

function debug($arr)
{
    echo '<pre>' . print_r($arr, true) . '</pre>';
}