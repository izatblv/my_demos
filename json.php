<?php
require "my_functions.php";
?>
<!doctype html>
<html lang="en">
<?php head("Json demo") ?>
<body>
<div class="container">
    <h1>Json demo</h1>
    <div>
        <h5>Массив</h5>
        <button id="add_array">Добавить</button>
        <br><br>
        <form action="" method="post">
            <p id="form_array">

                <button type="submit">Отправить</button>
            </p>
        </form>
    </div>
    <h5>Результат Json</h5>
    <?php
    $i = 1;
    foreach ($_POST as $key => $value) {
        global $last_key;
        if ($i == 1) {
            $last_key = $value;
        }
        if ($i == 2) if ($last_key == "" && $value == "") continue;
        if ($i == 2) {
            if ($last_key != "") $ar[$last_key] = $value; else array_push($ar, $value);
        }
        $i++;
        if ($i > 2) $i = 1;
    }

    d($ar);
    json_save($ar, 'test');

    ?>
    <a href="/test.json" download>
        <button>Скачать Json</button>
    </a>

</div>
</body>
<script>
    var i = 0;
    $("#add_array").click(function () {
        i++;
        // $("#form_array").before("<p>ключ "+i+": <input type=\"text\" name=\"key"+i+"\" /> значение "+i+": <input type=\"text\" name=\"value"+i+"\" /></p>");
        $("#form_array").before("<div class=\"row\"><div class=\"col\"><input type=\"text\" class=\"form-control\" placeholder=\"Key "+i+"\" name=\"key"+i+"\" /></div><div class=\"col\"><input type=\"text\" class=\"form-control\" placeholder=\"Value "+i+"\" name=\"value"+i+"\" ></div></div><br>");
    });
    $("#add_array").trigger("click");
    $("button").addClass('btn');
</script>
</html>